\chapter{Background Corrections}

Having investigated the necessary corrections for the detector performance deficiencies that affect all of the particle species similarly in chapter 5, we now turn our attention to corrections that apply specifically to each particle species. Collectively the corrections addressed in this chapter are called background corrections because they aim to remove particles identical to or indistinguishable from the particle of interest but which were produced by mechanisms other than directly from the medium resulting from the heavy-ion collision. In the following sections we discuss the general methodology used to investigate the backgrounds relevant to each species and how the magnitude of the effects are quantified.

\section{General Methodology: The Simulation Chain}
Unless otherwise noted in the sections below, all of the background corrections were determined via the simulation chain shown in figure \ref{fig:SimulationChain}. First, Au+Au heavy-ion events are simulated for each collision energy using the \href{https://urqmd.org/}{Ultrarelativistic Quantum Molecular Dynamics}, UrQMD \cite{UrQMD1,UrQMD2}, model. The output of UrQMD is a formatted text file with a ``.f'' extension. This file is converted into ROOT trees for easier processing. As input, the detector simulator (GEANT3/STARSIM) requires the simulated event be formatted in the ``STAR new text format'', or .tx for short, and that they be assigned a vertex location within the geometry of the detector. Thus, the second step in the chain is to create these files from the ROOT trees. The vertex location of each event is chosen by sampling the $z$- and $xy$-vertex distributions obtained from data. Third, the simulated events are then passed through STARSIM resulting in a ``Zebra'' (``.fzd'') file. This file contains all of the information about how the detector responded to the simulated event. Fourth, the Zebra files are processed by the reconstruction chain (BFC) to create ROOT trees in the ``minimc'' format. This is the same format which is used for the embedding analysis in the previous chapter. Fifth, the centrality of the events is determined using the same methodology outlined in chapter 3, that is: the events are characterized by either \textit{RefMult}, \textit{RefMultPosY}, or \textit{RefMultNegY} depending on their $z$-vertex locations; the quantities are corrected for how the acceptance of the detector changes with the $z$-vertex position; \textit{RefMultPosY} and \textit{RefMultNegY} are converted to \textit{RefMult}; and a Glauber Monte-Carlo and negative binomial particle production model are used to determine the centrality bin cuts. Finally, the simulated events in the minimc files are binned by centrality and analyzed using the methods discussed below to determine the background correction curves. The total number of reconstructed events for each combination of energy and event configuration are shown in table \ref{table:NumberOfSimEvents}

\begin{figure}[h!]
  \centering
  \includegraphics[width=.7\textwidth]{Images/YieldCorrection/SimulationChain.png}
  \caption{A schematic showing the steps in the simulation chain that result in background correction curves.}
  \label{fig:SimulationChain}
\end{figure}

\begin{table}[h]
\centering
\begin{tabular}{lccc}
\multicolumn{4}{c}{\textbf{Number of Reconstructed Events}} \\ \hline
\textbf{$\sqrt{s_{NN}}$}           & \textbf{Center} & \textbf{PosY} & \textbf{NegY} \\ \hline \hline
\multicolumn{1}{l|}{\textbf{7.7}}  & 172,851     & 145,063     & 144,276    \\
\multicolumn{1}{l|}{\textbf{11.5}} & 176,529     & 145,449     & 145,206    \\
\multicolumn{1}{l|}{\textbf{14.5}} & 150,422     & 129,948     & 132,174    \\
\multicolumn{1}{l|}{\textbf{19.6}} & 148,507     & 117,079     & 114,283    \\
\multicolumn{1}{l|}{\textbf{27.0}} & 147,228     & NA          & NA         \\
\multicolumn{1}{l|}{\textbf{39.0}} & 103,449     & NA          & NA         \\
\multicolumn{1}{l|}{\textbf{62.4}} & 103,081     & NA          & NA      
\end{tabular}
\caption{The total number of reconstructed events in each simulated dataset. Note that a larger number of events is necessary at lower energy since there are fewer tracks per event.}
\label{table:NumberOfSimEvents}
\end{table}


The tools necessary to perform the multiple steps of the  simulation chain are provided by the author across four software repositories. The \textsc{StUrQMD} software repository, which is available here \href{https://bitbucket.org/chris_flores/sturqmd}{\url{https://bitbucket.org/chris_flores/sturqmd}}, permits the user to perform all of the steps in the chain up to and including the creation of the minimc files. The centrality determination software is again provided by the \textsc{GlauberMCModel} repository available here: \href{https://bitbucket.org/chris_flores/glaubermcmodel}{\url{https://bitbucket.org/chris_flores/glaubermcmodel}}. The source code needed to process the minimc files is provided in the \textsc{MiniMCReader} repository here: \href{https://bitbucket.org/chris_flores/minimcreader}{\url{https://bitbucket.org/chris_flores/minimcreader}}. Finally, the source code needed to determine the correction curves is provided as part of the \textsc{DavisDSTAnalysis} repository which is available here: \href{https://bitbucket.org/chris_flores/davisdstanalysis}{\url{https://bitbucket.org/chris_flores/davisdstanalysis}}.

\section{Pion Corrections}
The two primary sources of background for the pion spectra are muon contamination and decays of heavier particles into pions known as feed down. In the later case, decays can be divided into strong and weak categories. The strong decays occur on time scales short enough that the resulting pions would have come into existence prior to freeze-out. Hence, they carry useful information about the system and are not corrected for. The pions from weak decays, on the other hand, are created on longer time scales and do not carry information regarding the chemistry, thermal properties, or expansion of the system. The pions from weak decays must therefore be corrected for. In this section we discuss these two background sources, muon contamination and weak decays, and detail how the magnitude of their contribution to the pion spectra was estimated.

\subsection{Muon Contamination}
The similarity between the pion and muon masses results in the two species being indistinguishable from each other given the methods of particle identification available in this analysis. Most of the muons produced in heavy-ion collisions are the daughters of charged pion decays ($\pi^+ \rightarrow \mu^+ + \nu_{\mu}$ or $\pi^- \rightarrow \mu^- + \overline{\nu_{\mu}}$). These are the very pions which we wish to measure. At first glance then, it might be seem desirous to \textit{not} correct for the muon contamination since the number of measured pions plus the number of measured muons will be closer to the number of charged pions prior to any of their decays. However, the reconstruction efficiency for muons is not necessarily the same as for pions because the number of muons which make it into the final track sample is contingent on the global DCA cut and, most importantly, the charged pion decay is modeled by the detector simulator which means that the effect of the pion decay on their reconstruction efficiency is already accounted for. Consequently we must estimate the level of muon contamination in the pion spectra and adjust for its presence.

The muon contamination is estimated using the results of the simulation chain described above. Reconstructed muons which pass all of the track quality cuts in the analysis are binned by event centrality, charge, pion rapidity, and pion transverse mass as seen in figure \ref{subfig:MuonBackground}. Additionally, the sum of the reconstructed muons and pions, again all of which have passed the track quality cuts, are binned similarly and shown in figure \ref{subfig:PionAndMuonSum}. The muon contamination fraction in each bin is then defined as $\mu_{\textrm{Bkgd}} = N_{\mu}/\left (N_{\mu}+N_{\pi} \right )$, which is computed as the ratio of the histograms. An example of the muon background fraction at midrapidity and as a function of transverse mass for the most central 5\% of events in the Au+Au $\sqrt{s_{NN}} = 7.7$ GeV dataset is shown in figure \ref{fig:PionMuonContaminationBackground} for both positive and negative pions. It is observed that muon contamination correction is quite small, reaching approximately 1\% at its maximum. Despite the relatively large number of simulated events, after binning by centrality and rapidity, the results are unable to distinguish between a simple exponential and power law parameterizations. Generally the power law fit is found to have the lower $\chi^{2}$ of the two functions, thus it is used for the nominal correction of the spectra. The exponential fit and one sigma bands are used in the determination of the systematic errors associated with this correction as described below.


\begin{figure}[h!]
  \centering
  \captionsetup[subfigure]{width=.45\textwidth}
  \subfloat[Reconstructed muons.]{%                                                                                                 
    \includegraphics[width=.49\textwidth]{Images/YieldCorrection/ReconstructedMuons_07.pdf}
    \label{subfig:MuonBackground}
  }
  \hfill
  \subfloat[Sum of reconstructed pions and muons.]{%
    \includegraphics[width=.49\textwidth]{Images/YieldCorrection/ReconstructedPionsMuons_07.pdf}
    \label{subfig:PionAndMuonSum}
  }
  \caption{The numerator (\textit{a}) and denominator (\textit{b}) of the muon contamination background fraction as described in the main text for the top 5\% most central events (\textit{Center} Configuration) in the Au+Au $\sqrt{s_{NN}}=7.7$ GeV dataset.}
  \label{fig:MuonContaminationBackground}
\end{figure}

\begin{figure}[h!]
  \centering
  \captionsetup[subfigure]{width=.45\textwidth}
  \subfloat[$\pi^{+}$]{%
    \includegraphics[width=.49\textwidth]{Images/YieldCorrection/AuAu07_ColliderCenter_PionPlus_cent08_yIndex20_MuonContamination.pdf}
    \label{subfig:PionPlusMuonBackground}
  }
  \hfill
  \subfloat[$\pi^{-}$]{%
    \includegraphics[width=.49\textwidth]{Images/YieldCorrection/AuAu07_ColliderCenter_PionMinus_cent08_yIndex20_MuonContamination.pdf}
    \label{subfig:PionMinusMuonBackground}
  }
  \caption{Examples of the muon contamination background fraction at midrapidity for $\pi^{+}$ (\textit{a}) and $\pi^{-}$ (\textit{b}) as a function of transverse mass for the top 5\% most central events (\textit{Center} Configuration) in the Au+Au $\sqrt{s_{NN}}=7.7$ GeV dataset. The red and blue curves are exponential and power law fits to the data respectively.}
  \label{fig:PionMuonContaminationBackground}
\end{figure}
\clearpage

\subsection{Feed Down}
In addition to being thermally created by the medium resulting from the heavy-ion collisions, pions can also be the decay daughters of heavier particles. The majority of these pions are produced by weakly-decaying neutral mesons and various hyperons such as lambdas. Since the kinematics of these pions are defined by a secondary process they do not carry information about the dynamics of the medium and so their contribution to the pion spectra must be removed.  

The results of the simulation chain are used to identify pions that come from weak decay processes. Events are binned by centrality and tracks identified as pions are binned by charge, rapidity, transverse mass, and parent ID. The parent ID signifies which particle species decayed to result in each pion. The results of this process are shown in figure \ref{fig:ParentIDContributionExamplesPion} for a single transverse mass bin at midrapidity for the 5\% most central events from the Au+Au $\sqrt{s_{NN}} = 7.7$ GeV dataset. Pions with no parent ID (shown as NA) were created from the heavy-ion collision itself. All other pions are the decay products of their respective parents. The feed down background fraction is defined as $\pi_{\textrm{bkgd}} = N_{\textrm{decay}}/N_{\textrm{total}}$, where $N_{\textrm{Decay}}$ is the number of pions coming from weak decays and $N_{\textrm{Total}}$ is the total number of pions from all sources (histogram entries).

Figure \ref{fig:FeedDownContributionExamplesPion} shows examples of the feed down background fractions as a function of transverse mass at midrapidity for positive and negative pions from the most central 5\% of events in the Au+Au $\sqrt{s_{NN}} = 7.7$ GeV dataset. The results are parameterized with exponential and power law fits. Although the $\chi^{2}$ values of both functions generally indicate good agreement with the data, it is found that the exponential function consistently describes the shape of the low transverse mass region better. Hence, we use the exponential function as the default correction curve and use the power law and one sigma bands in the estimation of the systematic error associated with this correction, as described below.

\begin{figure}[h!]
  \centering
  \captionsetup[subfigure]{width=.45\textwidth}
  \subfloat[$\pi^{+}$]{%                                                                                                 
    \includegraphics[width=.49\textwidth]{Images/YieldCorrection/PionPlusFeedDownBackground_07_MidY_SinglemT.pdf}
    \label{subfig:PionPlusFeedDownSinglemT}
  }
  %\hfill
  \subfloat[$\pi^{-}$]{%
    \includegraphics[width=.49\textwidth]{Images/YieldCorrection/PionMinusFeedDownBackground_07_MidY_SinglemT.pdf}
    \label{subfig:PionMinusFeedDownSinglemT}
  }
  \caption{The distribution of parent particles that decay into $\pi^{+}$ (\textit{a}) and $\pi^{-}$ (\textit{b}) for a single transverse mass bin at midrapidity for the top 5\% most central events (\textit{Center} Configuration) in the Au+Au $\sqrt{s_{NN}}=7.7$ GeV dataset.}
  \label{fig:ParentIDContributionExamplesPion}
\end{figure}

\begin{figure}[h!]
  \centering
  \captionsetup[subfigure]{width=.45\textwidth}
  \subfloat[$\pi^{+}$]{%                                                                                                 
    \includegraphics[width=.49\textwidth]{Images/YieldCorrection/AuAu07_ColliderCenter_PionPlus_cent08_yIndex20_FeedDown.pdf}
    \label{subfig:PionPlusFeedDownFraction}
  }
  %\hfill
  \subfloat[$\pi^{-}$]{%
    \includegraphics[width=.49\textwidth]{Images/YieldCorrection/AuAu07_ColliderCenter_PionMinus_cent08_yIndex20_FeedDown.pdf}
    \label{subfig:PionMinusFeedDownFraction}
  }
  \caption{Examples of the pion feed down fraction as a function of transverse mass at midrapidity for $\pi^{+}$ (\textit{a}) and $\pi^{-}$ (\textit{b}) for the top 5\% most central events (\textit{Center} Configuration) in the Au+Au $\sqrt{s_{NN}}=7.7$ GeV dataset. The red and blue curves are exponential and power law fits respectively.}
  \label{fig:FeedDownContributionExamplesPion}
\end{figure}

\clearpage


\section{Proton Corrections}
Protons also suffer from two main sources of background. The first source is the result of the protons present in the detector material itself and the second is the decay products of hyperons. In this section we discuss these two sources and the techniques used to estimate their contributions to the proton spectra.

\subsection{Feed Down}

Just as some of the measured pions are from decays of other particles, so too are some of the protons and anti-protons. In the case of the (anti)protons the decaying particles in question are primarily hyperons. For example, common decay modes of the $\Lambda$ and $\Sigma$ hyperons that result in the background of interest are $\Lambda^{0} \rightarrow p+\pi^{-}$, $\Sigma^{+} \rightarrow p + \pi^{0}$, $\overline{\Lambda^{0}} \rightarrow \bar{p}+\pi^{+}$, and $\overline{\Sigma^{-}} \rightarrow \bar{p} + \pi^{0}$. The background due to these decays is quantified using the same technique that was used for pions. The events and tracks resulting from the simulation chain must first pass the event and track cuts and are then binned by centrality, rapidity and transverse mass. Figure \ref{fig:ProtonParentIDContributionExamples} shows the parent ID distribution of p and $\mathrm{\bar{p}}$ for a particular transverse mass bin at midrapidity and for events in the top 5\% of centrality from the $\sqrt{s_{NN}}$ = 62.4 GeV dataset. Clearly a significant number of the measured protons and anti-protons come from the decays. Since the momenta of these tracks are defined by secondary processes their contribution to the spectra must be removed. 

\subsection{Knockout}
As energetic particles travel through the detector they can collide with nuclei in the material. Such collisions can result in a proton being ``knocked out'' of the nucleus and be reconstructed as a track. Some of these tracks will satisfy all of the track quality cuts used in this analysis and will therefore be included in the proton spectra. Clearly, these protons carry no information about the medium produced in the heavy-ion collision and therefore their contribution to the spectra must be removed.

The magnitude of the background due to knockout proton contribution depends on two factors. Higher multiplicity events will result in larger knockout proton backgrounds because there are simply more tracks moving through the detector and changes in the material budget of the detector can result in more or less knockout protons depending on the material density. Fortunately, the kinematics of the collisions that result in knockout protons are such that the protons typically have low momentum and therefore significant bending in the magnetic field. The result is that most knockout protons will be accurately tracked as having relatively large global DCA values. Thus the most effective cut for eliminating the knockout proton contribution is the global DCA cut.

Previous analyses (\cite{62GeVSTARSpectra,lokeshAnalysisNote,ropponThesis}) have used a global DCA cut of 3.0 cm and found that the knockout background can approach 40\% in the low $m_{T}-m_{0}$ portion of the spectrum. To reduce this background this analysis used a global DCA cut of 1.0 cm. The remaining background in this analysis was evaluated using the results of the simulation chain. Figure \ref{fig:ProtonParentIDContributionExamples} shows the parent ID distribution of p and $\mathrm{\bar{p}}$ for a particular transverse mass bin at midrapidity and for events in the top 5\% of centrality from the $\sqrt{s_{NN}} = 62.4$ GeV dataset. Note that in addition to the feed down protons originating from the $\Lambda$ and $\Sigma$ decays as discussed previously, there are also protons which have pions and kaons as parents. These protons are the result of the energetic pions and kaons undergoing knockout reactions with the detector material. Since there are no anti-protons in the detector material none can be knocked out and hence the parent GEANT ID distribution of the anti-protons is devoid of such entries.

\subsection{Combined Feed Down and Knockout Background}

Since both the feed down and knockout backgrounds of the protons can be evaluated using the same technique, we combine the two backgrounds to be the total background due to secondary processes. The background fraction is determined for each centrality, rapidity, and transverse mass bin from histograms such as those found in figure \ref{fig:ProtonParentIDContributionExamples}. The background fraction is computed as $p_{\textrm{bkgd}} = N_{\textrm{secondary}}/N_{\textrm{total}}$. The quantity $p_{\textrm{bkgd}}$ is the background fraction of $p$ or $\bar{p}$, the quantity $N_{\textrm{secondary}}$ represents the entries of the parent ID histogram that have a parent ID (i.e. not NA). The quantity $N_{\textrm{total}}$ represents the total number of entries in the histogram. Figure \ref{fig:FeedDownContributionExamplesProton} shows the background fraction for p and $\bar{p}$ at midrapidity as a function of transverse mass for events from the top 5\% of centrality from the $\sqrt{s_{NN}} = 62.4$ GeV dataset. The background fraction is parameterized using both power law and and exponential fits. The exponential fit is chosen as the default fit for correction and the power law fit and the confidence intervals are used in the processes of estimating the systematic error.

\begin{figure}[h!]
  \centering
  \captionsetup[subfigure]{width=.45\textwidth}
  \subfloat[p]{%                                                                                                 
    \includegraphics[width=.45\textwidth]{Images/YieldCorrection/ProtonPlusFeedDownBackground_62_MidY_SinglemT.pdf}
    \label{subfig:ProtonFeedDownSinglemT}
  }
  %\hfill
  \subfloat[$\mathrm{\bar{p}}$]{%
    \includegraphics[width=.45\textwidth]{Images/YieldCorrection/ProtonMinusFeedDownBackground_62_MidY_SinglemT.pdf}
    \label{subfig:ProtonFeedDownSinglemT}
  }
  \caption{The distribution of parent particles that result in protons (\textit{a}) and  anti-protons (\textit{b}) for a single transverse mass bin at midrapidity for the top 5\% most central events (\textit{Center} Configuration) in the Au+Au $\sqrt{s_{NN}}=62.4$ GeV dataset. Note that the protons with pions and kaons as parents are the result of knockout collisions.}
  \label{fig:ProtonParentIDContributionExamples}
\end{figure}

%%%%

\begin{figure}[h!]
  \centering
  \captionsetup[subfigure]{width=.45\textwidth}
  \subfloat[p]{%                                                                                                 
    \includegraphics[width=.45\textwidth]{Images/YieldCorrection/ProtonPlusFeedDownBackground_62_MidY.pdf}
    \label{subfig:ProtonPlusFeedDownFraction}
  }
  %\hfill
  \subfloat[$\mathrm{\bar{p}}$]{%
    \includegraphics[width=.45\textwidth]{Images/YieldCorrection/ProtonMinusFeedDownBackground_62_MidY.pdf}
    \label{subfig:ProtonMinusFeedDownFraction}
  }
  \caption{Examples of the proton (\textit{a}) and anti-proton (\textit{b}) feed down fraction - and knockout background in the case of protons - as a function of transverse mass at midrapidity for the top 5\% most central events (\textit{Center} Configuration) in the Au+Au $\sqrt{s_{NN}}=62.4$ GeV dataset.}
  \label{fig:FeedDownContributionExamplesProton}
\end{figure}

\clearpage

\section{Kaon Corrections}
The feed down background contributions for kaons were investigated using the same techniques used for pions and protons. However, negligible background is observed as can be seen in figure \ref{fig:ParentIDContributionExamplesKaon}. The figure shows the parent ID of kaons for a single transverse mass bin at midrapidity for central events in the $\sqrt{s_{NN}} = 62.4$ GeV dataset, where the background should be the largest. The negligible kaon background found in this analysis is consistent with the observations of previous spectra analyses at STAR. As a result no background corrections are needed for kaons. 
\begin{figure}[h!]
  \centering
  \captionsetup[subfigure]{width=.45\textwidth}
  \subfloat[$K^{+}$]{%                                                                                                 
    \includegraphics[width=.49\textwidth]{Images/YieldCorrection/AuAu62_ColliderCenter_KaonPlus_cent08_yIndex20_mTm004_FeedDown.pdf}
    \label{subfig:KaonPlusFeedDownSinglemT}
  }
  %\hfill
  \subfloat[$K^{-}$]{%
    \includegraphics[width=.49\textwidth]{Images/YieldCorrection/AuAu62_ColliderCenter_KaonMinus_cent08_yIndex20_mTm004_FeedDown.pdf}
    \label{subfig:KaonMinusFeedDownSinglemT}
  }
  \caption{The distribution of parent particles that decay into $K^{+}$ (\textit{a}) and $K^{-}$ (\textit{b}) for a single transverse mass bin at midrapidity for the top 5\% most central events (\textit{Center} Configuration) in the Au+Au $\sqrt{s_{NN}} = 62.4$ GeV dataset. The background is expected to be the largest at this collision energy.}
  \label{fig:ParentIDContributionExamplesKaon}
\end{figure}
