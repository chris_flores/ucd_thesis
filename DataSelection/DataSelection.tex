\chapter{Data Collection, Storage, Selection and Binning}
Several aspects of this analysis present logistical challenges that encumber accessing and analyzing the necessary data. Most of the challenges arise simply as a result of the scale of this analysis. To wit, we intend to measure the yield of soft particles for all BES energies binned by species and event centrality, over as much kinematic phase space as possible in two dimensions, and for three event configurations per energy. This means that the overwhelming majority of events and tracks that are collected during the physics running of the accelerator are included in this analysis. This has two effects. First, whenever the data needs to be reprocessed, a large amount of time and computational resources must be invested. And second, because the yield extraction process requires at least one histogram for each bin, trying to bin all of the data in one pass results in a minimum of approximately 250,000 histograms. This results is a memory footprint well over the permissible amount for a single instance of a user's job at the RHIC Computing Facility (RCF).

To overcome the challenge of working with such large data sets the author has developed a reduced data file format which is designed to be compact, in the sense that it only contains information which is needed for the spectra analysis, and topologically flat, in the sense that its internal data structure has as little depth as possible. The first feature ensures that the needed disk space for their storage is as minimal as possible. The second feature greatly accelerates the read performance.  We call these reduced data files \textsc{DavisDsts}. In addition, the author has developed a complementary data \textit{API} (Application Programming Interface) which includes numerous routines designed to make interacting with the \textsc{DavisDsts} as easy and fast as possible. In this chapter we discuss the flow of data from the detector to the \textsc{DavisDsts} and the various cuts applied to the events and tracks used in this analysis.

\section{Data Collection}
The data used in this analysis were obtained by the STAR Collaboration as part of the Beam Energy Scan (BES) at the Relativistic Heavy Ion Collider (RHIC) located at Brookhaven National Laboratory in 2010, 2011, and 2014. During the physics running the data are streamed from the various detector subsystems to the data acquisition system where dedicated hardware known as ``event builders'' aggregate the raw signals obtained during each trigger into multiple binary files called ``DAQ'' files. These files are then transported to a High Performance Storage System (HPSS), a large magnetic-tape collection, for permanent storage. When the data-taking run is completed the performance of the detector during the run is studied and assembled into calibration libraries. When this is completed the data are ``produced,'' meaning the raw signals stored in the DAQ files are interpreted into measurements that can be used in physics analyses. It is during the production of the data where the calibrations are applied.

The result of the data production is the standard STAR data format known as the MuDst. These files are designed to be the lowest common denominator for physics analyses in STAR. As a result the MuDst files contain an enormous amount of information - far more than is needed for any one physics analysis. As noted in Chapter 2, the subdetectors of interest for this analysis are the Time Projection Chamber (TPC) which is used for tracking and particle identification via ionization energy loss and the Time of Flight (TOF) detector which is used for particle identification via measurements of a particle's velocity. Hence, even though most of the events and tracks \textit{cannot} be rejected for this analysis, most of the information associated with those events and tracks \textit{can} be excluded. It is the \textsc{DavisDst} data format that was designed for the purpose of containing the remaining, needed information.

The \textsc{DavisDsts} are produced from the MuDsts using the \textsc{StDataCollector} package also written by the author and available here: \url{https://bitbucket.org/chris_flores/stdatacollector/}. A user is able to specify several event and run level cuts in the \textsc{StDataCollector} package to perform an initial skimming. However, these are not analysis cuts and so will not be discussed here, with one exception. An important feature of the standard \textsc{StRefMultCorr} package is that it allows for bad run rejection. This feature was utilized during the production of the \textsc{DavisDsts} so that events from bad runs were rejected. The MuDst files used were obtained by querying the STAR file catalog. All queries have the following conditions in common: filetype=daq\_reco\_MuDst, tpx=1, filename$\sim$st\_physics, storage!=HPSS, nFiles=all. In addition, table \ref{table:FileCatalogQuery} contains the conditions that were applied for each energy and configuration.

Table \ref{table:FileSizeOnDisks} shows the required disk space to store the MuDst and \textsc{DavisDst} files. The large reduction in file size is accomplished by rejecting data from bad runs and keeping only the relevant event and track information for this spectra analysis.

\begin{table}[h]
\centering
\begin{tabular}{llllll}
\multicolumn{1}{c}{\textbf{$\sqrt{s_{NN}}$}} & \multicolumn{1}{c}{\textbf{STARVER}} & \multicolumn{1}{c}{\textbf{Prod. Tag}} & \multicolumn{1}{l}{\textbf{Target Setup}} & \multicolumn{2}{c}{\textbf{Run Range}} \\ \hline \hline
\textbf{7.7}                                 & \multicolumn{1}{l|}{\textbf{SL10h}}  & P10ih                            & AuAu7\_production                         & 11114074           & 11147025          \\
\textbf{11.5}                                & \multicolumn{1}{l|}{\textbf{SL10h}}  & P10ih                            & AuAu11\_production                        & 11148039           & 11158044          \\
\textbf{14.5}                                & \multicolumn{1}{l|}{\textbf{SL14i}}  & P14ii                            & production\_15GeV\_2014                   & 15053000           & 15070021          \\
\textbf{19.6}                                & \multicolumn{1}{l|}{\textbf{SL11d}}  & P11id                            & AuAu19\_production                        & 12113079           & 12122019          \\
\textbf{27.0}                                & \multicolumn{1}{l|}{\textbf{SL11d}}  & P11id                            & AuAu27\_production\_2011                  & 12172043           & 12179097          \\
\textbf{39.0}                                & \multicolumn{1}{l|}{\textbf{SL10h}}  & P10ik                            & AuAu39\_production                        & 11099102           & 11112023          \\
\textbf{62.4}                                & \multicolumn{1}{l|}{\textbf{SL10k}}  & P10ik                            & AuAu62\_production                        & 11080054           & 11098056         
\end{tabular}
\caption{The STAR Library version number and parameters of the queries made to the STAR file catalog for each energy during the conversion of the MuDsts to \textsc{DavisDsts}.}
\label{table:FileCatalogQuery}
\end{table}



\begin{table}[h]
\centering
\begin{tabular}{cccc}
\multicolumn{4}{c}{\textbf{Disk Space Requirements}} \\ \hline
\textbf{$\sqrt{s_{NN}}$} & \textbf{MuDsts (GB)} & \textbf{DavisDsts (GB)} & \textbf{\% Reduction} \\ \hline \hline
\multicolumn{1}{c|}{\textbf{7.7}} & 4010 & 102 & 97.4 \\
\multicolumn{1}{c|}{\textbf{11.5}} & 6386 & 452 & 92.9 \\
\multicolumn{1}{c|}{\textbf{14.5}} & 39140 & 994 & 97.5 \\
\multicolumn{1}{c|}{\textbf{19.6}} & 20798 & 1319 & 93.6 \\
\multicolumn{1}{c|}{\textbf{27.0}} & 36335 & 2864 & 92.1 \\
\multicolumn{1}{c|}{\textbf{39.0}} & 48634 & 4247 & 91.3 \\
\multicolumn{1}{c|}{\textbf{62.4}} & 86264 & 4325 & 94.9 \\ \hline \hline
\multicolumn{1}{c|}{\textbf{Total}} & 241567 & 14303 & 94.1
\end{tabular}
\caption{The cumulative disk space required for storage of the MuDst and DavisDsts files in GB. The large reduction in size is accomplished by rejecting bad runs and only keeping event and track information needed for this analysis.}
\label{table:FileSizeOnDisks}
\end{table}

\section{Event Selection}
Several event level cuts are applied to ensure the events observed in this analysis are of sufficient quality. The event cuts used in this analysis are summarized in table \ref{table:EventCuts} and the reasoning behind each cut is explained in the following subsections. Table \ref{table:NumberOfEvents} shows the number of events that pass the event selection criteria for each energy and event configuration.

\subsection{Event Configuration}
Note that for the lowest four energies, three event configurations are measured: \textit{Center}, \textit{PosY}, and \textit{NegY}. The \textit{Center} event configuration describes events near the center of the TPC while the \textit{PosY}(\textit{NegY}) configuration describe events located with very negative (positive) $z$-vertex locations with tracks measurable in the positive (negative) rapidity direction. The additional event configurations are chosen because their highly displaced vertex positions extend the rapidity acceptance of the detector and therefore the measurable range of the rapidity density distributions sought in this analysis.

The displaced vertex configurations are only applicable to the lowest four BES energies because, for these energies, RHIC was used as a storage ring. That is, because the Radio-Frequency (RF) cavities in RHIC were not accelerating the ion bunches, the Coulombic repulsion along the $z$ axis results in a more longitudinally diffuse bunch. This in turn increases the probability of having an Au+Au collision near the edges of the detector permitting a large enough sample of events to perform a spectra analysis.

\subsection{Longitudinal Vertex Selection, $V_{z}$}
 In principle, the $z$-vertex range should be selected to optimize the competing effects of increasing the number of events and minimizing the change in the detector performance with respect to the $z$ position. All event configurations measuring events at the center of the TPC have a symmetric $z$-vertex cut of $\pm30$ cm with the exception of the 7.7 GeV data, which is permitted to extend to $\pm50$ cm. The $z$-vertex ranges for all energies were chosen to be consistent with other BES spectra analyses. The larger range of the 7.7 GeV dataset is chosen to further increase the event statistics at that energy because: it has relatively few events compared to other energies; the number of tracks produced per collision at 7.7 GeV is fewer than any other energy; and the $z$-vertex distribution is widest of any energy. The last reason is again the result of the accelerator performance: namely, the lower beam energy has a smaller $\gamma$ factor resulting in even more longitudinally diffuse bunches.

 The $z$-vertex range of the \textit{PosY} and \textit{NegY} event configurations are chosen to maximize the rapidity acceptance of the detector while optimizing the number of events.

\subsection{Radial Vertex Selection, $V_{r}$}
A radial vertex selection is necessary to prevent including events that were the result of errant ions interacting with the beam pipe or detector material. The radial vertex cut is centered around the transverse location of the beam spot which varies slightly from energy to energy due to beam tuning. All event configurations for a given energy share the same radial vertex cut since the transverse location of the beam spot does not change significantly over the $z$-vertex range of STAR.

Note that the radial vertex locations for all energies and configurations are required to satisfy $V_{r}\le2.0$ cm except for the 14.5 GeV dataset, which is required to satisfy $V_{r}\le0.5$ cm. The tighter cut at 14.5 GeV is required because of the installation of a narrower beam pipe during that year in support of the Heavy-Flavor Tracker (HFT) program.

\subsection{ToF-Matched Tracks}
The drift time of the TPC is approximately 40 $\mu s$ which, because it is long compared to the bunch crossing rate, makes it susceptible to out-of-time pile-up events. These are events which occurred either immediately before or after the triggered event. To reduce the possible contamination of these events we require that the TOF detector - which is considerably faster than the TPC - have hits which match to some minimum number of tracks in the TPC. The minimum number of tracks must be small so as not to bias the most peripheral centrality bin, but large enough to be effective at rejecting the out-of-time events. It is found that requiring at least three ToF matched tracks is optimal. 

\subsection{Trigger ID Selection}
All of the events used in this analysis are required to have satisfied the minimum-bias (MB) trigger conditions at each energy. For the BES energies the MB trigger conditions are defined as coincidences between the East and West Beam-Beam Counter (BBC) detectors. Note that not all available MB triggers are used in this analysis. This is because some trigger IDs have too few events to perform the centrality determination procedure in Chapter 3. 

\begin{table}[h]
\centering
\begin{tabular}{lccc}
\multicolumn{4}{c}{\textbf{Number of Events}} \\ \hline
\textbf{$\sqrt{s_{NN}}$}           & \textbf{Center} & \textbf{PosY} & \textbf{NegY} \\ \hline \hline
\multicolumn{1}{l|}{\textbf{7.7}}  & 2,935,338                    & 344,303                            & 295,654                          \\
\multicolumn{1}{l|}{\textbf{11.5}} & 6,495,494                     & 970,595                            & 831,065                          \\
\multicolumn{1}{l|}{\textbf{14.5}} & 8,478,525                    & 517,602                            & 643,044                          \\
\multicolumn{1}{l|}{\textbf{19.6}} & 14,898,550                   & 1,816,303                          & 1,605,536                        \\
\multicolumn{1}{l|}{\textbf{27.0}} & 30,093,280                   & NA                                 & NA                               \\
\multicolumn{1}{l|}{\textbf{39.0}} & 97,091,040                   & NA                                 & NA                               \\
\multicolumn{1}{l|}{\textbf{62.4}} & 50,956,350                   & NA                                 & NA                              
\end{tabular}
\caption{The total number of events in each dataset satisfying all event selection criteria for each collision energy and event configuration.}
\label{table:NumberOfEvents}
\end{table}

\begin{sidewaystable}[p!]
\centering
\begin{tabular}{ccccccc}
\multicolumn{7}{c}{\textbf{Event Selection Criteria}} \\ \hline
\textbf{$\sqrt{s_{NN}}$} & Config. & $V_{z}$ (cm)& $|V_{r}|$ (cm) & Beam Spot ($x,y$) (cm) & \# ToF-Matched Tracks & Trigger IDs  \\ \hline \hline

\multicolumn{1}{l|}{\textbf{7.7}} & \begin{tabular}{c}Center \\PosY \\NegY \end{tabular}  & \multicolumn{1}{c|}{\begin{tabular}{c}$[-50,50]$ \\$[-200,-100]$ \\$[100,200]$ \end{tabular}} & $[0,2.0]$ & (0.2,-0.25) & $\ge3$ & \begin{tabular}{c}290001 \\290004 \end{tabular} \\ \hline

\multicolumn{1}{l|}{\textbf{11.5}} & \begin{tabular}{c}Center \\PosY \\NegY \end{tabular} & \multicolumn{1}{c|}{\begin{tabular}{c}$[-30,30]$ \\$[-200,-100]$ \\$[100,200]$ \end{tabular}}  & $[0,2.0]$ & (0.1,-0.23) & $\ge3$ & 310014 \\ \hline

\multicolumn{1}{l|}{\textbf{14.5}} & \begin{tabular}{c}Center \\PosY \\NegY \end{tabular} & \multicolumn{1}{c|}{\begin{tabular}{c}$[-30,30]$ \\$[-200,-100]$ \\$[100,200]$ \end{tabular}}  & $[0,0.5]$ & (0.0,-0.90) & $\ge3$ & 440015 \\ \hline

\multicolumn{1}{l|}{\textbf{19.6}} & \begin{tabular}{c}Center \\PosY \\NegY \end{tabular} & \multicolumn{1}{c|}{\begin{tabular}{c}$[-30,30]$ \\$[-200,-100]$ \\$[100,200]$ \end{tabular}}  & $[0,2.0]$ & (0.37,-0.03) & $\ge3$ & \begin{tabular}{c}340001 \\340011 \\340021 \end{tabular} \\ \hline

\multicolumn{1}{l|}{\textbf{27.0}} & Center & \multicolumn{1}{c|}{$[-30,30]$} & $[0,2.0]$ & (0.13,-0.07) & $\ge3$ & 360001 \\ \hline

\multicolumn{1}{l|}{\textbf{39.0}} & Center & \multicolumn{1}{c|}{$[-30,30]$} & $[0,2.0]$ & (0.30,-0.07) & $\ge3$ & 280001 \\ \hline

\multicolumn{1}{l|}{\textbf{62.4}} & Center & \multicolumn{1}{c|}{$[-30,30]$} & $[0,2.0]$ & (0.39,-0.06) & $\ge3$ & \begin{tabular}{c}270001 \\270011 \\270021 \end{tabular} \\ \hline
\end{tabular}
\caption{Event selection criteria for each energy and event configuration. Note that the radial vertex cut is centered with respect to the beam spot location and that all trigger IDs are permitted for any event configuration.}
\label{table:EventCuts}
\end{sidewaystable}

\clearpage

\section{Track Selection}
It is also necessary to apply track cuts to ensure that only high quality tracks are included in the analysis. Tables \ref{table:TrackCuts} and \ref{table:TofTrackCuts} summarize the track cuts used in this analysis. The cuts in the first table are required to be satisfied for all tracks. The cuts in the second table are required for a track's TOF hit information to be considered in the analysis. The cuts are explained in the following subsections.

\begin{table}[h!]
\centering
\begin{tabular}{ccccc}
\multicolumn{5}{c}{\textbf{Track Selection Criteria}}                                           \\ \hline
\textbf{Track Flag} & \textbf{Fit Points} & \textbf{dE/dx Points} & \textbf{Fit/Possible Points} & \textbf{glDCA (cm)} \\ \hline \hline
$[0,1000)$          & $\ge 15$            & $\ge 10$              & $\ge .52$  & $\le 1.0$                   
\end{tabular}
\caption{Criteria tracks must satisfy to be considered for this analysis.}
\label{table:TrackCuts}
\end{table}

\begin{table}[h!]
\centering
\begin{tabular}{cccc}
\multicolumn{4}{c}{\textbf{Additional Criteria for TOF Candidate}}                     \\ \hline
\textbf{Match Flag} & \textbf{$1/\beta$} & \textbf{yLocal (cm)} & \textbf{zLocal (cm)} \\ \hline \hline
$\ge 1$             & $\ge 0$            & $[-1.6,1.6]$    & $[-2.8,2.8]$   
\end{tabular}
\caption{Additional criteria applied to TOF matched candidate tracks to ensure a valid time-of-fight measurement.}
\label{table:TofTrackCuts}
\end{table}

\subsection{Track Flag}
The track reconstruction algorithm is able to assign descriptive flag values to the tracks during their reconstruction. A track's flag is a four digit number: $zxyy$. The first digit, $z$, gives pile up information and is either 1 for a pile up track or 0 otherwise. The second digit, $x$, indicates which detectors were used in the track refitting. The value can range from 1 (TPC only) to 9. Although this digit can be useful to gather information, it is not generally useful for quality cuts. The last pair of digits, $yy$, gives information about the status of the helix refit. There are several possible values for the pair, but they can be interpreted as either positive (good refit) or negative (bad refit). The sign of the last pair of digits is applied to the whole flag and thus only tracks that have positive flag values less than 1000 are permitted for this analysis. For a more detailed description of all the possible values of the track flag see the comments at the top of the StTrack source code: \url{http://www.star.bnl.gov/webdata/dox/html/StTrack_8h_source.html}.

\subsection{Fit Points}
The number of fit points of a track describes the number of ($x,y,z$) coordinates that are used in the helix fit. It is important to consider that multiple detector subsystems can contribute hits that can be used in a track's reconstruction. For this analysis the term ``Fit Point'' will be used to mean fit point as measured \textit{by the TPC}. As described in Chapter 2, there are a total of 45 readout pads in each sector of the TPC -- 13 in the inner part of the sector and 32 in the outer. This means that a track can have a maximum of 45 hit points. By default a primary track must have at least 5 hit points which is the number of parameters in the helix fit. During track reconstruction the reconstruction algorithm can throw away some of the hit points if they are outliers. The remaining points are used in the helix fit and are therefore called fit points. 

The minimum number of fit points required for a track to be considered must be chosen thoughtfully. The momentum resolution of the track improves with more fit points, but requiring too many fit points will limit the low $p_{T}$ acceptance. A minimum of 15 fit points was determined to be a good value for this analysis. It is large enough to require hits from both the inner and outer sectors and small enough not to substantially limit the low $p_{T}$ acceptance of the detector. The latter is an important consideration in this analysis since the majority of the particle yield is at low $p_{T}$ and because the low $p_{T}$ acceptance can change with rapidity.

\subsection{dE/dx Points}
Each hit point of a track in the TPC has an associated $dE/dx$ measurement. Thus, a track may have up to 45 measurements of its ionization energy loss. As before, two competing factors must be taken into consideration to determine the minimum number of hit points used in the $dE/dx$ calculation. First, the $dE/dx$ resolution improves with a larger number of measurements. Second, as described in chapter 2, a 70\% truncated mean method is used to find the average value of a track's $dE/dx$. This means that roughly one-third of a track's $dE/dx$ measurements are discarded. Hence, the minimum number of hit points used in the $dE/dx$ determination should not approach the minimum number of fit points, but should be sufficiently many to provide good $dE/dx$ resolution. It was found that a minimum of 10 $dE/dx$ points was a good balance. The cut provides sufficient resolution and is two-thirds of the minimum number of fit points.

\subsection{Ratio of Fit Points to Possible Points}
A somewhat common problem that can occur during the track reconstruction process results from a single real track being reconstructed as two tracks. This can happen if the hits in one region of the detector are not associated with the hits in another region. These are known as split tracks. Tracks that pass through the central membrane are most prone to this issue, but the problem can also arise if hits in the outer sectors are not associated with hits in the inner sectors. To supress this problem it is common to require that the ratio of the number of hits used in the helix fit be greater than half, 52\% to be precise, of the possible number of hits the track should have had based on its trajectory. 

\subsection{Global Distance of Closest Approach (DCA)}
Spectral analyses of light particles, such as this, endeavor to report the spectra of particles directly produced by the medium resulting from the heavy-ion collision. As such it is necessary to try to ensure that the tracks included in the analysis come from a single heavy-ion collision and that they are not the result of long-lived particles which decay to light particles or particles that resulted from secondary collisions in the detector material. Corrections must be made to fully account for these effects, but requiring that a track's distance of closest approach to the primary vertex be small removes many of these confounding tracks, which may have been the result of pile up or decays. A common choice of cut to achieve these results is a maximum global DCA of 1 cm. The global DCA must be used since the primary DCA is defined to be 0 cm in the track refitting procedure. We use the 1 cm cut in this analysis.

\subsection{ToF Match Flag}
After tracks are reconstructed using the hit information from the TPC, a matching algorithm is used to pair hits observed in the Time-of-Flight (ToF) detector with the tracks observed in the TPC. The ToF match flag quantity ranges from zero, if the track has no matched ToF hit, to 3, if the track could have caused multiple hits observed in the ToF. For this analysis, as is done in other analyses, we require that the ToF match flag simply be nonzero to indicate that the track has a candidate hit in the ToF detector.

\subsection{Inverse Beta}
The inverse measure of a track's velocity provided by the ToF detector is required to be greater than zero for this analysis. The primary purpose of this cut is to remove the tracks that have an invalid velocity measurement (the value would be negative by default). One may be tempted to strictly require that the measurement be in the physical range bounded below by the speed of light at $1/\beta=1$, but in practice we find that this is neither necessary nor advantageous. It is not necessary since very few tracks have super-luminal velocities that are valid. It is not advantageous because the tracks that do have such velocities are typically high momentum tracks, in which case the measurement resolution combined with the uncertainty in the start time calculation permits super-luminal velocities.

\subsection{Local ToF Pad Coordinates}
Each ToF pad has its own local coordinate system and can report the location of each hit relative to the pad origin. Cuts are made in the local coordinate space to ensure that the hit is near the center of the active volume of each pad. The purpose of this cut is to reduce the overall number of mismatches that cause background during the yield extraction process. ToF mismatches occur when the $dE/dx$ of a track in the TPC suggests it is of a particular species while the $1/\beta$ measured in the ToF suggests it is another species. The mismatch rate increases the further the hit distance is from the center of the ToF pad so limits are placed on the hit location.


\section{Data Binning}
The data used in this analysis and that have passed the above quality cuts are binned in several dimensions. Events are first binned into event configurations using their z-vertex locations and then binned in centrality using the centrality bin determination procedure discussed in Chapter 3. It was decided that this analysis would utilize nine centrality bins divided into percentages of the total cross section: 0-5, 5-10, 10-20, 20-30, 30-40, 40-50, 50-60, 60-70, and 70-80\%. The tracks belonging to events within each centrality bin are then binned by mass assumption ($\pi$,$k$,$p$), rapidity (bin width, $\Delta y=0.1$), and in $m_{T}-m_{0}$ (bin width, $\Delta(m_{T}-m_{0})=0.025$ GeV).
