\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{List of Figures}{vii}{chapter*.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{List of Tables}{xxi}{chapter*.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Abstract}{xxiii}{chapter*.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Acknowledgments}{xxiv}{chapter*.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Ultra-Relativistic Heavy-Ion Physics}{2}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.1}Theoretical Foundations (1950-1990)}{2}{subsection.1.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.2}Experimental Progress and the Onset of Deconfinement (1990-2008)}{6}{subsection.1.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.1.2.1}The Kink, Horn, and Step - The Signs of Marek}{6}{subsubsection.1.1.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.1.2.2}The Dale}{9}{subsubsection.1.1.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.3}The Modern Era and QGP Signatures (2000-Present)}{11}{subsection.1.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.1.3.1}Collective Behavior: Elliptic Flow, Perfect Fluidity, and $n_{q}$ Scaling}{12}{subsubsection.1.1.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.1.3.2}Hadron and Jet Suppression}{16}{subsubsection.1.1.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.1.3.3}Quarkonia Suppression}{19}{subsubsection.1.1.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}The Beam Energy Scan (2010-Present)}{22}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.1}The Phase Diagram of Nuclear Matter}{22}{subsection.1.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.2.1.1}Baryon Stopping}{23}{subsubsection.1.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.2.1.2}System Evolution and Phase Trajectory}{25}{subsubsection.1.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.2}The Turn Off of QGP Signatures}{26}{subsection.1.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.3}Phase Change Phenomena}{27}{subsection.1.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.4}Critical-Point Fluctuations}{29}{subsection.1.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Overview of Work in This Thesis}{32}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.1}Paper Proposal Page}{33}{subsection.1.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Experimental Apparatus}{34}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}The Relativistic Heavy-Ion Collider Facility}{34}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Acceleration Procedure}{35}{subsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}The RHIC Design}{35}{subsection.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}The STAR Detector}{38}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}The Time Projection Chamber}{39}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.1.1}Track Reconstruction}{41}{subsubsection.2.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.1.2}Particle Identification via Energy Loss}{43}{subsubsection.2.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}The Time of Flight Detector System}{44}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.2.1}The Upgraded Vertex Position Detector and Start Time}{44}{subsubsection.2.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.2.2}The Barrel Time of Flight Detector}{45}{subsubsection.2.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.2.3}TPC Track to TOF Hit Reconstruction}{46}{subsubsection.2.2.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.2.4}Particle Identification via Time Of Flight}{47}{subsubsection.2.2.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Event Centrality Determination}{50}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Data Collection and Event Selection for \textsc {StRefMultExtendedCorr}}{52}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Centrality Variables}{54}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Acceptance Correction - Reweighting as a Function of $z$-Vertex Location}{55}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Conversion of $RefMultPosY$ and $RefMultNegY$ to $RefMult$}{56}{section.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}Centrality Bin Determination}{60}{section.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.1}Glauber Monte Carlo Model}{60}{subsection.3.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.5.1.1}Inelastic Nucleon-Nucleon Cross Section}{61}{subsubsection.3.5.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.5.1.2}Nucleon Distributions}{63}{subsubsection.3.5.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.5.1.3}Glauber Monte Carlo Model Results}{67}{subsubsection.3.5.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.2}Particle Production Model}{70}{subsection.3.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.3}Glauber Model + NB Fits to Multiplicity Distributions}{73}{subsection.3.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.4}Centrality Binning}{75}{subsection.3.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.6}Study of Systematic Errors}{76}{section.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.7}Tabulated Centrality Bins}{79}{section.3.7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Data Collection, Storage, Selection and Binning}{85}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Data Collection}{86}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Event Selection}{88}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Event Configuration}{88}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Longitudinal Vertex Selection, $V_{z}$}{89}{subsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}Radial Vertex Selection, $V_{r}$}{89}{subsection.4.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.4}ToF-Matched Tracks}{90}{subsection.4.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.5}Trigger ID Selection}{90}{subsection.4.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Track Selection}{93}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}Track Flag}{93}{subsection.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.2}Fit Points}{94}{subsection.4.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.3}dE/dx Points}{94}{subsection.4.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.4}Ratio of Fit Points to Possible Points}{95}{subsection.4.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.5}Global Distance of Closest Approach (DCA)}{95}{subsection.4.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.6}ToF Match Flag}{96}{subsection.4.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.7}Inverse Beta}{96}{subsection.4.3.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.8}Local ToF Pad Coordinates}{96}{subsection.4.3.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Data Binning}{97}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Detector Performance Corrections}{98}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Errors of Efficiency Calculations}{99}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}TOF Matching Efficiency}{99}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Embedding}{101}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.1}Track Energy-Loss Correction}{102}{subsection.5.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.2}Finite Bin Width}{103}{subsection.5.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.3}TPC Tracking Efficiency}{104}{subsection.5.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Background Corrections}{107}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}General Methodology: The Simulation Chain}{107}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Pion Corrections}{109}{section.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2.1}Muon Contamination}{110}{subsection.6.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2.2}Feed Down}{112}{subsection.6.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3}Proton Corrections}{114}{section.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.1}Feed Down}{114}{subsection.6.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.2}Knockout}{114}{subsection.6.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.3}Combined Feed Down and Knockout Background}{115}{subsection.6.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.4}Kaon Corrections}{117}{section.6.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Particle Yield Extraction}{118}{chapter.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}PID Calibration: Part 1}{119}{section.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.2}Track Binning and Recentering}{121}{section.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.3}PID Calibration: Part 2}{124}{section.7.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.4}Intra-Bin Transverse Mass Location}{126}{section.7.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.5}Yield Extraction}{130}{section.7.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.5.1}$Z_{\textrm {TPC}}$ Yields}{130}{subsection.7.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.5.2}$Z_{\textrm {TOF}}$ Yields}{131}{subsection.7.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.5.3}Probabilistic Yield Extraction}{132}{subsection.7.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.6}Application of Corrections}{135}{section.7.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.6.1}Background Fraction Corrections}{135}{subsection.7.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.6.2}Finite Bin Width Correction}{136}{subsection.7.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.6.3}Energy Loss Correction}{137}{subsection.7.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.6.4}Tracking Efficiency Correction}{137}{subsection.7.6.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.7}Treatment of Systematic Errors}{137}{section.7.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.7.1}Systematic Error on the Uncorrected Yield}{138}{subsection.7.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.7.2}Systematic Error on the Corrected Yield}{139}{subsection.7.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.7.2.1}General Methodology}{140}{subsubsection.7.7.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.7.2.2}Variation of Individual Corrections}{144}{subsubsection.7.7.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {8}Results and Analysis}{146}{chapter.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.1}Spectral Model Fits}{146}{section.8.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.1.1}General Fit Methodology}{147}{subsection.8.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.1.2}Pions}{148}{subsection.8.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.1.3}Kaons}{152}{subsection.8.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.1.4}Protons}{155}{subsection.8.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.2}Transverse Mass Spectra}{158}{section.8.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.3}Rapidity Density Distributions}{201}{section.8.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.4}Full Phase Space Yields of $\pi ^{\pm }$}{209}{section.8.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.5}The Dale Observable}{212}{section.8.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.6}Energy Dependence of the Most Central, Midrapidity Results}{213}{section.8.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.6.1}Most Central, Midrapidity Spectra}{213}{subsection.8.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.6.2}Spectral Model Parameters}{217}{subsection.8.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.6.3}Particle Yields and Ratios}{220}{subsection.8.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.6.4}Coulomb Analysis}{223}{subsection.8.6.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {9}Conclusion}{226}{chapter.9}
