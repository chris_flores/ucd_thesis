\chapter{Experimental Apparatus}
The data used in this analysis were collected during the Beam Energy Scan Program in 2010, 2011, and 2014 using the Solenoidal Tracker at RHIC (STAR) which is an experiment on the ring of the Relativistic Heavy-Ion Collider (RHIC) located at Brookhaven National Laboratory (BNL). The laboratory itself is a multidisciplinary facility that supports several fundamental fields of science such as biology, chemistry, and physics, as well as several applied science and engineering programs including computer science, nanomaterials, and energy production. In this chapter the accelerator and detector used to collect data for this analysis are generally introduced. Details of subsystems are also discussed when they are pertinent to either the data collection or analysis.

\section{The Relativistic Heavy-Ion Collider Facility}
The RHIC facility is a multi-part accelerator complex that is capable of accelerating a range of nuclei to relativistic speeds. A schematic of the facility, drawn from a bird's eye view, can be seen in Figure \ref{fig:rhicComplex}. The facility was designed to provide $\sqrt{s_{NN}}=200$ GeV Au+Au collisions and polarized $p+p$ collisions at $\sqrt{s}=500$ GeV. Since this analysis focuses solely on Au+Au collisions the remainder of the discussion will pertain to RHIC as a heavy-ion facility.

\subsection{Acceleration Procedure}
To achieve the high energies required, the Au nuclei are accelerated in stages as described in \cite{rhicOverview} and summarized here. The process begins with the vaporization of solid gold using a sputter ion source. The ion source adds an electron, giving the gold atom an overall negative charge. This negative beam is extracted into a Tandem Van de Graaff accelerator which accelerates the ions to 1 MeV and strips off 33 electrons giving Au\textsuperscript{32+}. The ions then reach the booster synchrotron via the heavy-ion transfer line. The booster accelerates the ions to approximately 1 GeV and separates the ions into six bunches. When the ions exit the booster they are incident on a stripping foil, which removes all but two electrons, resulting in Au\textsuperscript{77+}. The booster cycle is repeated four times so that the Alternating Gradient Synchrotron (AGS) can be filled with 24 bunches. The AGS first debunches and rebunches the ions so that there are four final bunches and then accelerates the ions up to the RHIC injection energy. The final two electrons are stripped as the ions are transferred to RHIC. The entire process is repeated up to 14 times so that RHIC can be filled with 56 bunches each with $10^{9}$ ions.

The injection energy of the ions from the AGS into RHIC depends on the required operating procedure. If RHIC is meant to provide Au+Au collisions at $\sqrt{s_{NN}}=19.6$ GeV or below, then the injection energy is $\sqrt{s_{NN}}/2$ GeV. During these running configurations RHIC then serves as a storage ring for the duration of the data collection time. If collisions higher than $\sqrt{s_{NN}}=19.6$ are required then the injection energy is 9.8 GeV and the ions are further accelerated by RHIC until the desired energy is reached.

\subsection{The RHIC Design}
RHIC itself is an intersecting synchrotron type accelerator that was designed with remarkable scientific flexibility in mind. A synchrotron, like a cyclotron, is a class of cyclic accelerator. Cyclic accelerators are typified by designs which recycle beams around a ring so that acceleration of the beam can take place over multiple revolutions. These accelerators require both bending magnets to steer the beam around the ring and an electric field to provide acceleration. The synchrotron and cyclotron designs differ in the particular implementation of bending magnets and electric fields.

Cyclotrons have a single, large, constant magnetic field oriented perpendicular to the plane of beam rotation that is provided by two large ``D'' (named for their shape) electromagnets. The small gap between the ``D'' magnets is filled with a radio frequency oscillated electric field so that the particle is accelerated twice during each revolution. A particle is injected into the cyclotron at its center and as the particle's velocity increases it spirals outward eventually being ejected tangentially. Although simple in design, the top energy in a cyclotron is inherently limited by the size and strength of the single, large magnetic field, since it must encompass the entire plane of beam rotation.  

Synchrotrons, such as RHIC, bend the beams around a ring using several dipole magnets that are configured in a ``racetrack'' orientation. That is, the ring of a synchrotron is really an n-sided regular polygon, where the corner sections of the polygons contain the dipole magnets responsible for bending the beams, and the straight sections house the experimental detectors. Acceleration is provided by radio frequency (RF) cavities in one or more of the straight sections. Synchrotrons derive their name from the acceleration process which requires that the magnetic fields of the bending magnets be increased synchronously with the momentum of the beam.

RHIC is hexagonal in shape as can be seen in Figure \ref{fig:rhicComplex}, with a circumference of approximately 3.8 km. The six straight sides of the ring have been used for the RF cavities, a service building, and four experiments: STAR, PHENIX, PHOBOS, and BRAHMS which were located at the 6, 8, 10, and 2 o'clock positions respectively. At the time of this writing (June 2017) only STAR remains as an operational detector. However, plans to resume operations at the PHENIX interaction region using a new detector dubbed sPHENIX are well underway.

\begin{figure}[p]
  \centering 
  \includegraphics[width=.8\textwidth]{Images/Experiment/RHIC_Complex.png} 
  \caption{A schematic of the RHIC accelerator complex layout obtained from reference \cite{rhicOverview}.}
  \label{fig:rhicComplex}  
\end{figure}

When RHIC began operations in 2000 it was the highest energy heavy-ion collider in the world and remains the most powerful source of polarized proton beams for spin studies. As previously noted it was designed with remarkable scientific flexibility in mind. From the heavy-ion perspective this flexibility is derived from two unique design characteristics. First, RHIC is a dual-ring intersecting synchrotron which means it is capable of simultaneously accelerating beams of different species in opposite directions and steering them into collisions. Thus it can provide collisions between asymmetric systems such as p+A. This capability is important because understanding how results of heavy-ion experiments differ from those of a superposition of $p+p$ collisions can offer crucial insights about the produced medium. Second, it was designed to be capable of accelerating multiple nuclear species to various energies between injection energy and its top energy. The variety of energies permits results to be understood as a function of collision energy starting from the energies of its predecessors. This capability is also important because it allows us to probe various regions of the nuclear matter phase space as described in chapter one. It is the second capability that is essential for the Beam Energy Scan program and therefore in this analysis.

\section{The STAR Detector}
The Solenoidal Tracker at RHIC (STAR) detector, shown in Figure \ref{fig:starSchematic}, was built purposefully to study heavy-ion collisions. This means that it must provide tracking and particle identification over a large solid angle and in high-multiplicity environments. The multiple subsystems and half-Tesla magnet of the detector are designed to satisfy these requirements over a geometric region covering the full $2\pi$ in azimuth and $|\eta|\le1.5$ in pseudorapidity. In addition, the subsystems are designed to exhibit (with few exceptions) both azimuthal and longitudinal symmetry. 

The coordinate system of the detector is described in \cite{starCoordinateSystem} and is defined as being right-handed with respect to the collider. Its origin is defined to be the center of the STAR solenoid. The positive x-axis points radially outward from the center of RHIC (approximately south). The positive y-axis points upward - away from the center of the earth. Finally, the positive z-axis points in the same direction as the clockwise circulating beam as it passes through the detector (approximately west). The azimuthal angle, $\phi=[-\pi,\pi]$, is measured in the x-y plane and symmetrically from the positive x-axis. The polar angle, $\theta=[0,\pi]$, is measured from the positive z-axis.

The present analysis requires the tracking and particle identification capabilities of the STAR detector. Tracking is provided by the Time Projection Chamber (TPC) and particle identification is provided by both the TPC and the Time of Flight (TOF) detector. As a result the following discussion will focus on the details of these subsystems in particular. If the reader desires a more general introduction to the STAR detector as a whole we defer to \cite{starOverview}. 

\begin{figure}[h]
  \centering 
  \includegraphics[height=.5\textwidth]{Images/Experiment/STAR_Detector.png} 
  \caption{A 3D CAD rendering of the STAR detector courtesy of \cite{alexSchmah}.}
  \label{fig:starSchematic}  
\end{figure}

\subsection{The Time Projection Chamber}
The Time Projection Chamber (TPC) is the defining subsystem of the STAR detector. It is a cylindrical volume spanning 4.2 m in length and has an inner radius of 50 cm and outer radius of 200 cm. Its length is coaxial with and encloses the beam pipe. A schematic of the subdetector can be seen in Figure \ref{fig:tpcSchematic}. A more detailed description of the TPC is available in \cite{tpcOverview}. A summary of the most important operational details is provided here for completeness.

The volume of the TPC is filled with P10 gas (90\% Argon, 10\% Methane). The gas in the TPC is kept slightly above atmospheric pressure so that oxygen and water vapor do not enter sensitive volume and oxidize the surfaces. The P10 gas was chosen because its main constituent, the argon, is extremely stable which limits spurious ionizations and is relatively inexpensive - a necessary consideration for such a large volume, and has a very low affinity for free electrons. The methane is present as an absorber of energy. Its relatively large mass and multiple degrees of freedom (rotational, vibrational, etc.)  allow it to absorb kinetic energy from: drifiting electrons - giving them a constant drift velocity, ionized argon atoms - resulting in their minimal drift distances, and UV photons - preventing confounding avalanches.

The large volume of gas is divided along the z-axis into two drift chambers by a central membrane which is kept at 28 kV relative to the endcaps, which are grounded. The resulting electric field of about 133 V/cm is kept uniform along the z-axis by inner and outer field cages which are composed of several conductive rings connected with high precision resistors. The electric field results in a constant electron drift velocity of 5.45 cm/$\mu$s.

\begin{figure}[h]
  \centering 
  \includegraphics[height=.55\textwidth]{Images/Experiment/TPC_Schematic.png} 
  \caption{A schematic of the STAR TPC obtained from reference \cite{tpcOverview}.}
  \label{fig:tpcSchematic}  
\end{figure}


The readout electronics of the TPC are installed in a total of 24 pie-shaped Multi-Wire Proportional Chambers (MWPC) that divide each half of the TPC into 12 sectors in azimuth. A schematic of a single sector is shown in Figure \ref{fig:sectorSchematic}. Each sector is divided radially into an inner sector and outer sector. The inner sector has 13 pad rows while the outer sector has 32. This means that each track can have its position and energy loss recorded up to 45 times. The direction of the wiring in the MWPCs was chosen to give the best momentum measurements for high $p_{T}$ tracks and so are oriented perpendicular to the direction such a track would travel.

\begin{figure}[h]
  \centering 
  \includegraphics[height=.5\textwidth]{Images/Experiment/TPCSector_Schematic.png} 
  \caption{A schematic of a single anode sector of the TPC obtained from \cite{tpcOverview}. The inner portion of the sector is on the right and the outer is on the left.}
  \label{fig:sectorSchematic}  
\end{figure}

\subsubsection{Track Reconstruction}
As the detector design suggests, the tracking function of the TPC is accomplished via gas ionization measurements. When a charged particle transits the gas it ionizes the Argon atoms. The resulting free electrons drift to the end caps of the TPC where they are collected into what is called a ``hit.'' The x,y location of the readout pad is used to determine the hit's transverse location and the drift time is used to determine its z location. The determination of the z location by measuring the electron drift time gives the TPC its name. Figure \ref{fig:driftAnimation} shows the status of free electrons at 15.2 $\mu$s after an event occurred. The free electrons have drifted from their ionization location toward their respective end caps.

The discrete hits observed in the TPC are interpreted into continuous tracks by a Kalman filtering process that involves iteratively fitting helices - the trajectory a charged particle in a uniform magnetic field is expected to have. The result of this reconstruction process is a set of global tracks. These tracks represent the trajectories of all the particles traveling through the detector after the trigger was satisfied. The next step in the process is to associate the global tracks together into one or more vertices, known as primary vertices, that represent the location of an A+A collision. This is achieved by the vertexing algorithm and involves extrapolating the fit helices to the z-axis. By observing the density of tracks as a function of $z$, a set of candidate vertices are formed. The $x$ and $y$ locations of the helix extrapolations are then taken into consideration to exclude some candidate vertices and to provide a higher-precision vertex-position measurement.

\begin{figure}[h]
  \centering 
  \includegraphics[height=.45\textwidth]{Images/Experiment/TPC_ElectronDrift.png} 
  \caption{A simulation of free electrons drifting from their ionization location to the anode plane where they are read out. This snapshot was taking 15.2 $\mu$s after the event occured. Full animation is available here: \url{https://www.star.bnl.gov/public/tpc/hard/tpcrings/simTPC.html} courtesy of \cite{geneVanBuren}.}
  \label{fig:driftAnimation}  
\end{figure}

Now that the global tracks have been associated with a vertex, they are refit with helices using the vertex position as the first point in the helix. This provides a more precise measure of momentum thereby improving the momentum resolution of the tracks. When this refitting procedure is finished the tracks are known as primary tracks because their trajectories now originate from the primary vertex. It is important to note that there can be multiple primary vertices per trigger. However, the vertex finding algorithm is able to rank the vertices and only the most highly ranked vertex in each trigger is used in this analysis.

Kinematic measurements of the track are obtained from the helix fit. All tracks are assumed to have unit charge. The sign of the charge is determined from the handedness of the helix. The track's transverse momentum is determined by the radius of curvature of the helix and the longitudinal momentum is determined by the helix pitch or turn density. 

\subsubsection{Particle Identification via Energy Loss}
Particle identification in the TPC is accomplished by taking advantage of the way particles lose energy when they move through matter. The physics of this process is described by the Bethe-Bloch equation (equation \ref{eq:bethbloch},\cite{pdg}) which predicts how much energy a particle will lose per unit of path length as a function of $\beta\gamma=p/m$. Particles of different masses, then, lose energy at different rates for a given value of $\beta\gamma$. Considerable effort was expended in \cite{bichsel} to understand the energy loss of particles in the P10 gas of the STAR TPC. This effort resulted in what are known as the Bichsel curves. 

\begin{flalign}
  \label{eq:bethbloch}
  \begin{split} 
  \frac{dE}{dx} = \; &Kz^{2}\frac{Z}{A}\frac{1}{\beta^{2}} \left[ \frac{1}{2}\log\left( \frac{2m_{e}c^{2}\beta^{2}\gamma^{2}T_{max}}{I^{2}}\right ) -\beta^{2} -\frac{\delta(\beta\gamma)}{2}\right ]  \\
  &A = \textup{Atomic Mass of Absorber (g/mol)}  \\ 
  &\frac{k}{A} = 4\pi N_{A}r_{e}^{2}m_{e}c^{2}/A \; \textup{(cm\textsuperscript{2}/g)} \\
  &N_{A} = \textup{Avogadro's Constant (1/mol)}  \\
  &r_{e} = \textup{Classical Electron Radius (fm)}  \\
  &m_{e} = \textup{Mass of Electron (MeV)}  \\
  &m_{0} = \textup{Mass of Incident Particle (MeV)}  \\
  &z = \textup{Atomic Number of Incident Particle}  \\
  &Z = \textup{Atomic Number of Absorber}  \\
  &T_{\textrm{max}} = \frac{2m_{e}c^{2}\beta^{2}\gamma^{2}}{1+2\gamma \left(m_{e}/m_{0}\right)+\left( m_{e}/m_{0}\right)^{2}} \\
  &\;\;\;\;\;\;\;= \textup{Max Energy Transfer (MeV)}  \\
  &I = \textup{Mean Excitation Energy (eV)} \\
  &\delta(\beta\gamma) = \textup{Density Effect Correction (Material Dependent)}
  \end{split}  
\end{flalign}
\\

The amount of energy a particle loses as it moves through and ionizes the gas of the TPC is proportional to the number of electrons measured for that track at each hit. Thus, by carefully calibrating the TPC a track's $dE/dx$ can be measured. However, in practice some care must be taken when computing the $dE/dx$ of a track. Recall that each track can have up to 45 hits associated with it and therefore up to 45 independent measurements of its $dE/dx$. The value of $dE/dx$ at each hit is a Landau distributed random variable. Since the Landau distribution has a long, high-side tail a simple average of the 45 measurements would be skewed toward large values of $dE/dx$. To overcome this, the largest 30\% of measurements are removed and the remaining are used to compute a 70\% truncated mean. Figure \ref{fig:tpcEnergyLoss} shows the measured $dE/dx$ of tracks as a function of total momentum. The curves in the figures are the theoretically expected energy loss values predicted by the Bichsel curves. Clearly the Bichsel curves are an excellent descriptor of how tracks lose energy in the TPC. Clear separation can be seen between the tracks of different species at low momentum, however the merger of the energy loss bands at high momenta require an alternative method of particle identification. 

\begin{figure}[h]
  \centering 
  \includegraphics[width=.85\textwidth]{Images/Experiment/TPC_EnergyLoss.pdf} 
  \caption{The energy loss ($dE/dx$) of tracks as measured in the TPC as a function of total momentum from the $\sqrt{s_{NN}}=7.7$ GeV data set. The curves in the figure are the Bichsel curves described in the text.}.
  \label{fig:tpcEnergyLoss}  
\end{figure}

\subsection{The Time of Flight Detector System}
Particle identification at higher momenta is provided by the Time Of Flight (TOF) and the Upgraded Pseudo Vertex Position (upVPD) detectors. Together these detectors are designed to act like a stopwatch that provides a measurement of the flight time of tracks. Both the upVPD and TOF systems are described in detail in \cite{tofProposal}, \cite{tofPerformance}, and \cite{tofSystem}. A summary of some of the important characteristics of the systems is included here.

\subsubsection{The Upgraded Vertex Position Detector and Start Time}
The upVPD system consists of two enclosures of extremely fast photomultipliers. As seen in Figure \ref{fig:starSchematic} there is an upVPD detector near the beam pipe positioned 5.7m from the center of the TPC on both sides of STAR. These cover $4.2<\eta<5.1$ and measure very forward photons from the collision and act as the start of the stopwatch. For energies less that $\sqrt{s_{NN}} = 39.0$ GeV it was found that the signals from the upVPDs were insufficient to determine the start time of the event. In these cases a ``startless'' method was used to determine the start time. This method assumes a pion mass for all tracks with $0.2 < p_{T} < 0.6$ GeV and that are within two standard deviations from the expected $dE/dx$ of a pion. This is an excellent assumption for this combination of $p_{T}$ and $dE/dx$. The mass assumption is combined with the momentum and track length measured in the TPC to estimate the start time for each track. The estimates are then averaged to determine the start time of the event. 

\subsubsection{The Barrel Time of Flight Detector}
The TOF system is a thin cylindrical shell that encloses the surface of the TPC. The detector system is composed of 120 trays, 60 for each the east and west half, of size 241.3 $\times$ 21.6 $\times$ 8.9 cm\textsuperscript{3}. Together the trays cover the full $2\pi$ in azimuth and $|\eta|\le0.9$. Each tray contains 32 Multi-Gap Resistive Plate Chambers (MRPC) set in a projective geometry so that tracks emanating from an average z-vertex location at the center of STAR will encounter them perpendicularly. A schematic of a single TOF tray with the projectively oriented MRPCs can be seen in Figure \ref{fig:tofTraySchematic}. 

\begin{figure}[h]
  \centering 
  \includegraphics[width=.85\textwidth]{Images/Experiment/TOFTray_Schematic.png} 
  \caption{A schematic of a single TOF tray with the projectively oriented MRPCs. This figure is taken from \cite{tofPerformance} Note that this figure contains 33 rather than 32 MRPCs because it was a prototype design.}.
  \label{fig:tofTraySchematic}  
\end{figure}

The internal structure of each of the MRPCs can be seen in Figure \ref{fig:tofPadSchematic}. It is composed of several resistive glass plates separated by gaps containing 95\% R-134a and 5\% isobutane gas. These plates are sandwiched between two graphite electrodes across which a high potential is applied. Finally, the electrodes are sandwiched between two layers of six copper readout pads. Hits are detected by gas ionizion measurements. As a track moves through the MRPC it ionizes the gas in the gaps between the plates creating electron avalanches. The resistive plates prevent the free electrons from streaming but are transparent to the electric fields created by the avalanches themselves. This transparancey allows the copper readout pads to measure the resulting image charge across the graphite electrode.

The readout pads are separated from each other by 0.3 cm and measure $3.15 \times 6.1$ cm\textsuperscript{2}. The isolation of the pads within each MRPC allow each pad to record a hit simultaneously. In addition, each pad is capable of providing the hit location with respect to its own coordinate system. This position discretion is taken advantage of in the track quality cuts discussed in Chapter 4. The pads and associated electronics are designed to provide a timing resolution for each hit of approximately 100 ps. However, in practice careful calibration and electronics correction often result in better timing resolution, down to 65 ps in the best cases. 

\begin{figure}[h]
  \centering 
  \includegraphics[width=.7\textwidth]{Images/Experiment/TOFPad_Schematic.png} 
  \caption{A schematic of the internal structure and pad layout within a single TOF MRPC. This figure is taken from \cite{tofProposal}}
  \label{fig:tofPadSchematic}  
\end{figure}

\subsubsection{TPC Track to TOF Hit Reconstruction}
Tracks measured and reconstructed in the TPC can now be matched with hits observed in the TOF pads. This is performed by a matching algorithm that extrapolates the trajectory of a TPC track to the radius of the TOF. The extrapolated positions are then associated with the observed TOF hits. The efficiency of the matching procedure is discussed in more detail in Chapter 5, but is generally between 50-60\% and is multiplicity dependent. Tracks in the TPC can be associated with multiple hits in the TOF and multiple tracks in the TPC can be associated with the same hit in the TOF. If a track is associated with one or more hits in the TOF detector it is said to have a ``match'' and the track is described as a ``TOF Matched Track.''

\subsubsection{Particle Identification via Time Of Flight}
Particle identification using the TOF detector is accomplished in one of two ways; by measurements of either velocity (equation \ref{eq:tofPIDInvBeta}) or mass squared (equation \ref{eq:tofPIDMassSquared}). Both methods require knowledge of the track's flight time and path length. The mass squared method also requires the track's momentum. As described, the TOF system gives the flight time and the TPC gives the momentum and path length.

In this analysis we utilize the velocity method because it does not introduce the uncertainties associated with the momentum determination into the particle identification methodology. This is especially useful since the errors on the momentum, path length, and flight time covary. Figure \ref{fig:TOFInverseBeta} shows the computed $1/\beta$ of tracks from measurements of their flight time and path length as a function of total momentum. The curves in the figure are the expected $1/\beta$ as a function of momentum and have been computed assuming each species' mass. The various species are clearly discernible over a wide range of momenta and become indistinguishable as their momenta become much larger than their mass and their velocities approach the speed of light.

\begin{flalign}
  \label{eq:tofPIDInvBeta}
  \frac{1}{\beta} = \; &\frac{\Delta t}{L} = \sqrt{\frac{m^{2}+p^{2}}{p^{2}}}  \\
  \label{eq:tofPIDMassSquared}
  m^{2} = \; &\frac{p^{2}}{\left( \beta \gamma \right)^{2}} = p^{2}\left( \frac{(c \Delta t)^{2}}{L^{2}} - 1\right ) \\
  \nonumber &\Delta t = \textup{Time of Flight}  \\ 
  \nonumber &L = \textup{Path Length} \\
  \nonumber &p = \textup{Momentum}  \\
  \nonumber &\gamma =  \sqrt{1/(1-\beta^{2})}
\end{flalign}

\begin{figure}[h]
  \centering 
  \includegraphics[width=.85\textwidth]{Images/Experiment/TOF_InverseBeta.pdf} 
  \caption{The $1/\beta$ computed using the time of flight measurements from the TOF detector as a function of total momentum from the $\sqrt{s_{NN}}=7.7$ GeV data set. The curves in the figure represent the expected values given a particle's mass.}
  \label{fig:TOFInverseBeta}  
\end{figure}
